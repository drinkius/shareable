//
//  HistoryTicketViewController.swift
//  parkour
//
//  Created by drinkius on 11/05/16.
//  Copyright © 2016 phirotto. All rights reserved.
//

import UIKit

class HistoryTicketViewController: UIViewController {
    
    @IBOutlet weak var ticketView: XibTicketView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var parkingName: UILabel!
    let dataBus = ViewDataBus.sharedInstance
    
    @IBAction func viewPricing(sender: AnyObject) {

        if let fee = sessionToShow.carpark?.fee_per_minute {
            if let currency = sessionToShow.carpark?.currency {
                showAlert("Fee per minute: \n\n \(currency) \(fee)")
            } else {
                showAlert("Couldn't fetch fee data")
            }
        } else {
            showAlert("Couldn't fetch fee data")
        }
    }
    
    @IBAction func needHelp(sender: AnyObject) {
        
        let stb = UIStoryboard(name: "Main", bundle: nil)
        let vc = stb.instantiateViewControllerWithIdentifier("support")
        self.navigationController!.pushViewController(vc, animated: true)
    }
    
    var sessionToShow: Session!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        sessionToShow = dataBus.historySessionToShow
        ticketView.apiUpdateSession(sessionToShow)
        parkingName.text = sessionToShow.carpark_name
        
        let currentDate = NSDate()
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateStyle = NSDateFormatterStyle.MediumStyle
        let enableDateString = dateFormatter.stringFromDate(currentDate)
        dateLabel.text = enableDateString
    }
    
    private func showAlert(message: String) {
        let alert = UIAlertController(title: message, message: nil, preferredStyle: .Alert)
        alert.addAction(UIAlertAction(title: "OK", style: .Cancel) { [unowned self] _ in
            
            })
        presentViewController(alert, animated: true, completion: nil)
    }
}
